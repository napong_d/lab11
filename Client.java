import com.lloseng.ocsf.client.AbstractClient;


public class Client extends AbstractClient {

	public Client(String host, int port) {
		super(host, port);
	}
	
	@Override
	protected void handleMessageFromServer(Object msg) {
		System.out.println("> " + msg);
	}
	
	


}
