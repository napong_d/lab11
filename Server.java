import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;


public class Server extends AbstractServer{

	private static final int PORT = 5555;
	private static final int LOGGEDIN = 1;
	private static final int LOGGEDOUT = 0;
	private static final int SEND = 2;

	private List<ConnectionToClient> clients = new ArrayList<ConnectionToClient>(); 
	private List<ConnectionToClient> nameclient = new ArrayList<ConnectionToClient>(); 

	public Server(int port) {
		super(port);

	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		//only allow string message. 
		if(!(msg instanceof String)){
			sendToClient(client,"unrecognized Message");
			return;
		}
		String message = (String) msg;
		int state = (Integer)client.getInfo("state");
		

		switch(state){
		case LOGGEDOUT:
			if(message.contains("Login")){
				String username = message.substring(6).trim();
				client.setInfo("username", username);
				client.setInfo("state", LOGGEDIN);
				super.sendToAllClients(username + " has been logged in");
			}
			else{
				sendToClient(client,"Please login");
				return;
			}
			break;


		case LOGGEDIN:
			if(message.contains("Logout")){
				client.setInfo("state", LOGGEDOUT);
				sendToClient(client,"You has been logged out");
				String username = (String) client.getInfo("username");
				super.sendToAllClients(username + " has been logged out");
			}
			
			else if(message.contains("To:")){
				String username = message.substring(4).trim();
				for(ConnectionToClient c : clients){
					if(c.getInfo("username").equals(username)){
						nameclient.add(c);
					}
				}
				client.setInfo("state",SEND);
			}
			
			else{
				String username = (String) client.getInfo("username");
				super.sendToAllClients(username + " : " + message );
			}
			break;
			
		case SEND: 
			if(message.contains("Logout")){
				client.setInfo("state", LOGGEDOUT);
				sendToClient(client,"You has been logged out");
				String username = (String) client.getInfo("username");
				super.sendToAllClients(username + " has been logged out");
				return;
			}
			
			for(ConnectionToClient c : nameclient){
				try {
					c.sendToClient(message);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	protected void sendToClient(ConnectionToClient client, String msg) {
		try{
			client.sendToClient(msg);
		}catch(IOException e){
			e.
			printStackTrace();
		}
		
		
	}
	
	public void clientConnected(ConnectionToClient client){
		clients.add(client);
		client.setInfo("state", LOGGEDOUT);
	}
	public synchronized void clientDisconnected(ConnectionToClient client){
		clients.remove(client);
	}

}
